$(document).ready(function(){
  soundManager.url = sm2_mod_url;
  soundManager.debugMode = false;
  soundManager.flashVersion = 9;
  soundManager.useMovieStar = true;
  soundManager.useHighPerformance = true; // position:fixed flash movie for faster JS/flash callbacks
  soundManager.wmode = 'transparent';
  
  soundManager.useFastPolling = true; // increased JS callback frequency, combined with useHighPerformance = true

  soundManager.flash9Options.useWaveformData = true;
  soundManager.flash9Options.useEQData = true;
  soundManager.flash9Options.usePeakData = true;
});
