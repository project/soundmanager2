<?php
?>
<div id='soundmanager2-outer'>
  <?php // print $variables['elements'];?>
  
  <div>
    <ul class="playlist">
      <?php  // Initialise an array for results
      $files = array();
      // Get the children
      $children = element_children($element);
      // Cycle through the file elements
      foreach ($children as $key) {
        // If nothing has been uploaded then there are items, but they are empty, so check they are set
        if (isset($element[$key]['#item']['filepath'])) {
          if (module_exists('drupalforfirebug')) {
            firep($element[$key]['#item']['filepath'], "files");
          }
          $files[] = $element[$key]['#item']['filepath'];
      ?>
        <li>
          <!--div class="ui360"-->
          <a href="/<?php print $element[$key]['#item']['filepath'] ;?>">
            <?php print ($element[$key]['#item']['data']['title'] ? $element[$key]['#item']['data']['title'] : $element[$key]['#item']['filename'] ) ;?>
          </a>
          <!--/div-->
        </li>
      <?php
        }
      }
      ?>
    </ul>
    <div id="control-template">
      <!-- control markup inserted dynamically after each link -->
      <div class="controls">
        <div class="statusbar">
          <div class="loading"></div>
          <div class="position"></div>
        </div>
      </div>
      <div class="timing">
        <div id="sm2_timing" class="timing-data">
          <span class="sm2_position">%s1</span> / <span class="sm2_total">%s2</span></div>
        </div>
        <div class="peak">
          <div class="peak-box">
            <span class="l"></span>
            <span class="r"></span>
          </div>
        </div>
      </div>
    
      <div id="spectrum-container" class="spectrum-container">
        <div class="spectrum-box">
          <div class="spectrum"></div>
        </div>
      </div> 
    
  </div>
</div>
